const mongoose = require('mongoose');
require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var esplistRouter = require('./routes/esp-list');
var espRouter = require('./routes/esp');
var espUdpRouter = require('./routes/esp-udp');
// var courselistRouter = require('./routes/course-list');
// var courseregistrationRouter = require('./routes/course-registration');
// var coursesRouter = require('./routes/courses');

var app = express();

mongoose.connect(process.env.MONGO_DB_URL, { useNewUrlParser: true, useUnifiedTopology: true });
const dbConnection = mongoose.connection;
dbConnection.on('error', (error) => console.error(error));
dbConnection.once('open', () => console.log("Połączonono z bazą"));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/esp-list', esplistRouter);
app.use('/esp', espRouter);
app.use('/udp', espUdpRouter);
// app.use('/course-list', courselistRouter);
// app.use('/course-registration', courseregistrationRouter);
// app.use('/courses', coursesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;