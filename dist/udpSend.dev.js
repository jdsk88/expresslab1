"use strict";

var PORT = 8888;
var HOST = '192.168.100.149';

var dgram = require('dgram');

var msg = new Buffer.from('["relays":[{"relay":"1","state":"ON"},{"relay":"2","state":"ON"},{"relay":"3","state":"ON"},{"relay":"4","state":"ON"}]');
var client = dgram.createSocket('udp4');
client.send(msg, 0, msg.length, PORT, HOST, function (err, bytes) {
  if (err) throw err;
  console.log('UDP msg sent to ' + HOST + ':' + PORT);
  client.close();
});