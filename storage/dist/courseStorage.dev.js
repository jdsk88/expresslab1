"use strict";

coursesArray = [];

exports.addCourse = function (course) {
  coursesArray.push(course);
};

exports.getAllCourses = function () {
  return coursesArray;
};