var PORT = 33333;
var HOST = '192.168.1.22';

var dgram = require('dgram');
var msg = new Buffer.from('{"deviceType":"iSwitch-4","frendlyName":"kitchen relays","ipAddress":"192.168.0.101","config":false,"relays":[{"state":"ON"},{"state":"ON"},{"state":"ON"},{"state":"ON"}],"isActive":1}');

var client = dgram.createSocket('udp4');
client.send(msg, 33333, msg.length, PORT, HOST, function(err, bytes) {
    if (err) throw err;
    console.log('UDP msg sent to ' + HOST + ':' + PORT);
    client.close();
});