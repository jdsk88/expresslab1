const mongoose = require("mongoose");
const esp = require("../models/esp");

exports.destroy_all = (req, res, next) => {
    esp.deleteMany({}, function(err) {
        if (err) {
            console.log(err);
        } else {
            res.end("success");
        }
    });
};