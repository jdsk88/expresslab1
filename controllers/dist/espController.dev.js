"use strict";

var mongoose = require("mongoose");

var esp = require("../models/esp");

exports.destroy_all = function (req, res, next) {
  esp.deleteMany({}, function (err) {
    if (err) {
      console.log(err);
    } else {
      res.end("success");
    }
  });
};