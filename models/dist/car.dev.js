"use strict";

var mongoose = require('mongoose');

var carShema = new mongoose.Schema({
  brand: {
    type: String,
    required: true,
    minlength: 3
  },
  model: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 15
  },
  buildYear: {
    type: Number,
    required: true,
    min: 2005
  },
  color: {
    type: String,
    required: false
  }
});
module.exports = mongoose.model('Car', carShema);