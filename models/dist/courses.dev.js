"use strict";

var mongoose = require('mongoose');

var config = require('config');

var jwt = require('jsonwebtoken');

var courseShema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3
  },
  code: {
    type: Number,
    required: true,
    minlength: 2,
    maxlength: 15
  },
  price: {
    type: Number,
    required: true,
    min: 2005
  },
  length: {
    type: Number,
    required: true
  }
});

courseShema.methods.generateAuthToken = function () {
  var token = jwt.sign({
    _id: this._id
  }, config.get('privatekey'));
  return token;
};

module.exports = mongoose.model('Course', courseShema);