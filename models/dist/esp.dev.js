"use strict";

var mongoose = require('mongoose');

var config = require('config');

var jwt = require('jsonwebtoken');

var espSchema = new mongoose.Schema({
  deviceType: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 15,
    unique: false
  },
  frendlyName: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 15,
    unique: true
  },
  ipAddress: {
    type: String,
    maxlength: 16,
    required: true,
    unique: true
  },
  relays: [{
    _id: false,
    state: String,
    unique: false
  }, {
    _id: false,
    state: String,
    unique: false
  }, {
    _id: false,
    state: String,
    unique: false
  }, {
    _id: false,
    state: String,
    unique: false
  }],
  createdAt: {
    type: Date,
    "default": Date.now()
  },
  isActive: {
    type: Boolean
  }
});

espSchema.methods.generateAuthToken = function () {
  var token = jwt.sign({
    _id: this._id
  }, config.get('privatekey'));
  return token;
};

module.exports = mongoose.model('Esp', espSchema); // relays: [{
//     _id: false,
//     // relay: String,
//     state: String,
// }, {
//     _id: false,
//     relay: String,
//     // state: String,
// }, {
//     _id: false,
//     relay: String,
//     // state: String,
// }, {
//     _id: false,
//     relay: String,
//     // state: String,
// }, ],