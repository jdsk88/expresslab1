const mongoose = require('mongoose');
const config = require('config');
const jwt = require('jsonwebtoken');
const courseShema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3
    },
    code: {
        type: Number,
        required: true,
        minlength: 2,
        maxlength: 15
    },
    price: {
        type: Number,
        required: true,
        min: 2005
    },
    length: {
        type: Number,
        required: true
    }
})


courseShema.methods.generateAuthToken = function() {
    const token = jwt.sign({ _id: this._id }, config.get('privatekey'));
    return token;
}
module.exports = mongoose.model('Course', courseShema);