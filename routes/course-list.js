var express = require('express');
var router = express.Router();

const Course = require('../models/courses');


router.get('/', async (req, res) => {
  try {
    const course = await Course.find()
    console.log(course);
    res.json(course)
  } catch (error) {
      res.status(500).json({ message: error.message })
  }
})

async function getCourse(req, res, next) {
  try {
      course = await Course.findById(req.params.id)
      if (null == course) {
          return res.status(404).json({ message: "Cannot find course with given id!" })
      }
  } catch (error) {
      res.status(500).json({ message: error.message })
  }
  res.course = course
  next();
}

router.get('/:id', getCourse, (req, res) => {
  res.json(res.course)
})

router.delete('/:id', getCourse, async (req, res) => {
  try {
      await res.course.remove()
      res.json({ message: " Course with given ID was removed" })
  } catch (error) {
      return res.status(500).status({ message: error.message })
  }
})

router.patch('/:id', getCourse, async (req, res) => {
  if (req.body.name != null) {
      res.car.name = req.body.name;
  }
  if (req.body.code != null) {
      res.car.code = req.body.code;
  }
  if (req.body.price != null) {
      res.car.price = req.body.price;
  }
  if (req.body.length != null) {
      res.car.length = req.body.length;
  }
  try {
      const updatedCourse = await course.save()
      res.json(updatedCourse)

  } catch (error) {
      return res.status(500).json({ message: error.message })
  }

})

module.exports = router;