var express = require('express');
var router = express.Router();

const Esp = require('../models/esp');
// const espController = require('../controllers/espController')



router.get('/', async(req, res) => {
    try {
        const esp = await Esp.find()
        console.log(esp);
        res.json(esp)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.get('/delete', async(req, res) => {
    try {
        const esp = await Esp.deleteMany({}, function(err) {
            if (err) {
                console.log(err);
            } else {
                res.end("all data was erased!");
            }
        });
        console.log(esp);
        res.json(esp)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})
router.get('/delete-one', getESP, async(req, res) => {
    try {
        const esp = await Esp.deleteOne();
        console.log(esp);
        res.json(esp)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})
async function getESP(req, res, next) {
    try {
        esp = await Esp.findById(req.params.id)
        if (null == esp) {
            return res.status(404).json({ message: "Cannot find esp with given id!" })
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
    res.esp = esp
    next();
}

router.get('/:id', getESP, (req, res) => {
    res.json(res.esp)
})

router.patch('/:id', getESP, async(req, res) => {
    if (req.body.frendlyName != null) {
        res.car.frendlyName = req.body.frendlyName;
    }
    if (req.body.ipAddress != null) {
        res.car.ipAddress = req.body.ipAddress;
    }
    if (req.body.config != null) {
        res.car.config = req.body.config;
    }
    if (req.body.state != null) {
        res.car.state = req.body.state;
    }
    try {
        const updatedESP = await esp.save()
        res.json(updatedESP)

    } catch (error) {
        return res.status(500).json({ message: error.message })
    }

})

module.exports = router;