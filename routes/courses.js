// const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

const Course = require('../models/courses');

// router.get('/', auth , async (req, res) => {
router.get('/', async(req, res) => {
    res.render('courseregistration', { title: 'course registration' });
})

router.post('/', async(req, res) => {
    const course = new Course({
        name: req.body.name,
        code: req.body.code,
        price: req.body.price,
        length: req.body.length
    })

    try {
        const newCourse = await course.save();
        // const token = course.generateAuthToken();
        // res.header('x-access-token', token).send({
        //     validated: true
        // });
        console.log(newCourse);
        res.render('saved', {
            title: "Your COURSE has been createed",
            courses: [newCourse]
        })
    } catch (error) {
        res.status(400).json({ message: error.message })
    }

})

module.exports = router