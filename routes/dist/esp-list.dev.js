"use strict";

var express = require('express');

var router = express.Router();

var Esp = require('../models/esp'); // const espController = require('../controllers/espController')


router.get('/', function _callee(req, res) {
  var _esp;

  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return regeneratorRuntime.awrap(Esp.find());

        case 3:
          _esp = _context.sent;
          console.log(_esp);
          res.json(_esp);
          _context.next = 11;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](0);
          res.status(500).json({
            message: _context.t0.message
          });

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 8]]);
});
router.get('/delete', function _callee2(req, res) {
  var _esp2;

  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return regeneratorRuntime.awrap(Esp.deleteMany({}, function (err) {
            if (err) {
              console.log(err);
            } else {
              res.end("all data was erased!");
            }
          }));

        case 3:
          _esp2 = _context2.sent;
          console.log(_esp2);
          res.json(_esp2);
          _context2.next = 11;
          break;

        case 8:
          _context2.prev = 8;
          _context2.t0 = _context2["catch"](0);
          res.status(500).json({
            message: _context2.t0.message
          });

        case 11:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[0, 8]]);
});
router.get('/delete-one', getESP, function _callee3(req, res) {
  var _esp3;

  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return regeneratorRuntime.awrap(Esp.deleteOne());

        case 3:
          _esp3 = _context3.sent;
          console.log(_esp3);
          res.json(_esp3);
          _context3.next = 11;
          break;

        case 8:
          _context3.prev = 8;
          _context3.t0 = _context3["catch"](0);
          res.status(500).json({
            message: _context3.t0.message
          });

        case 11:
        case "end":
          return _context3.stop();
      }
    }
  }, null, null, [[0, 8]]);
});

function getESP(req, res, next) {
  return regeneratorRuntime.async(function getESP$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return regeneratorRuntime.awrap(Esp.findById(req.params.id));

        case 3:
          esp = _context4.sent;

          if (!(null == esp)) {
            _context4.next = 6;
            break;
          }

          return _context4.abrupt("return", res.status(404).json({
            message: "Cannot find esp with given id!"
          }));

        case 6:
          _context4.next = 11;
          break;

        case 8:
          _context4.prev = 8;
          _context4.t0 = _context4["catch"](0);
          res.status(500).json({
            message: _context4.t0.message
          });

        case 11:
          res.esp = esp;
          next();

        case 13:
        case "end":
          return _context4.stop();
      }
    }
  }, null, null, [[0, 8]]);
}

router.get('/:id', getESP, function (req, res) {
  res.json(res.esp);
});
router.patch('/:id', getESP, function _callee4(req, res) {
  var updatedESP;
  return regeneratorRuntime.async(function _callee4$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          if (req.body.frendlyName != null) {
            res.car.frendlyName = req.body.frendlyName;
          }

          if (req.body.ipAddress != null) {
            res.car.ipAddress = req.body.ipAddress;
          }

          if (req.body.config != null) {
            res.car.config = req.body.config;
          }

          if (req.body.state != null) {
            res.car.state = req.body.state;
          }

          _context5.prev = 4;
          _context5.next = 7;
          return regeneratorRuntime.awrap(esp.save());

        case 7:
          updatedESP = _context5.sent;
          res.json(updatedESP);
          _context5.next = 14;
          break;

        case 11:
          _context5.prev = 11;
          _context5.t0 = _context5["catch"](4);
          return _context5.abrupt("return", res.status(500).json({
            message: _context5.t0.message
          }));

        case 14:
        case "end":
          return _context5.stop();
      }
    }
  }, null, null, [[4, 11]]);
});
module.exports = router;