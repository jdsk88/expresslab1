"use strict";

// const auth = require('../middleware/auth');
var express = require('express');

var router = express.Router();

var Esp = require('../models/esp'); // router.get('/', auth , async (req, res) => {


router.get('/', function _callee(req, res) {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          res.render('esp', {
            title: 'course registration'
          });

        case 1:
        case "end":
          return _context.stop();
      }
    }
  });
});
router.post('/', function _callee2(req, res) {
  var esp, newEsp;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          esp = new Esp({
            frendlyName: req.body.frendlyName,
            ipAddress: req.body.ipAddress,
            config: req.body.config,
            state: req.body.state
          });
          _context2.prev = 1;
          _context2.next = 4;
          return regeneratorRuntime.awrap(esp.save());

        case 4:
          newEsp = _context2.sent;
          // const token = course.generateAuthToken();
          // res.header('x-access-token', token).send({
          //     validated: true
          // });
          console.log(newEsp);
          res.render('saved', {
            title: "Your ESP has been added to database",
            esp: [newEsp]
          });
          _context2.next = 12;
          break;

        case 9:
          _context2.prev = 9;
          _context2.t0 = _context2["catch"](1);
          res.status(400).json({
            message: _context2.t0.message
          });

        case 12:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[1, 9]]);
});
module.exports = router; //axios from udp await and