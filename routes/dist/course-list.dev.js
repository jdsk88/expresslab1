"use strict";

var express = require('express');

var router = express.Router();

var Course = require('../models/courses');

router.get('/', function _callee(req, res) {
  var _course;

  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return regeneratorRuntime.awrap(Course.find());

        case 3:
          _course = _context.sent;
          console.log(_course);
          res.json(_course);
          _context.next = 11;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](0);
          res.status(500).json({
            message: _context.t0.message
          });

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 8]]);
});

function getCourse(req, res, next) {
  return regeneratorRuntime.async(function getCourse$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return regeneratorRuntime.awrap(Course.findById(req.params.id));

        case 3:
          course = _context2.sent;

          if (!(null == course)) {
            _context2.next = 6;
            break;
          }

          return _context2.abrupt("return", res.status(404).json({
            message: "Cannot find course with given id!"
          }));

        case 6:
          _context2.next = 11;
          break;

        case 8:
          _context2.prev = 8;
          _context2.t0 = _context2["catch"](0);
          res.status(500).json({
            message: _context2.t0.message
          });

        case 11:
          res.course = course;
          next();

        case 13:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[0, 8]]);
}

router.get('/:id', getCourse, function (req, res) {
  res.json(res.course);
});
router["delete"]('/:id', getCourse, function _callee2(req, res) {
  return regeneratorRuntime.async(function _callee2$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return regeneratorRuntime.awrap(res.course.remove());

        case 3:
          res.json({
            message: " Course with given ID was removed"
          });
          _context3.next = 9;
          break;

        case 6:
          _context3.prev = 6;
          _context3.t0 = _context3["catch"](0);
          return _context3.abrupt("return", res.status(500).status({
            message: _context3.t0.message
          }));

        case 9:
        case "end":
          return _context3.stop();
      }
    }
  }, null, null, [[0, 6]]);
});
router.patch('/:id', getCourse, function _callee3(req, res) {
  var updatedCourse;
  return regeneratorRuntime.async(function _callee3$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          if (req.body.name != null) {
            res.car.name = req.body.name;
          }

          if (req.body.code != null) {
            res.car.code = req.body.code;
          }

          if (req.body.price != null) {
            res.car.price = req.body.price;
          }

          if (req.body.length != null) {
            res.car.length = req.body.length;
          }

          _context4.prev = 4;
          _context4.next = 7;
          return regeneratorRuntime.awrap(course.save());

        case 7:
          updatedCourse = _context4.sent;
          res.json(updatedCourse);
          _context4.next = 14;
          break;

        case 11:
          _context4.prev = 11;
          _context4.t0 = _context4["catch"](4);
          return _context4.abrupt("return", res.status(500).json({
            message: _context4.t0.message
          }));

        case 14:
        case "end":
          return _context4.stop();
      }
    }
  }, null, null, [[4, 11]]);
});
module.exports = router;