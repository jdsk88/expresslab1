"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var axios = require('axios'); // const auth = require('../middleware/auth');


var express = require('express');

var router = express.Router();

var os = require('os');

var getHostIP = os.networkInterfaces();
var en0 = getHostIP.en0,
    eth0 = getHostIP.eth0;
var IPADDRESS = en0[1].address || eth0[0].address; // console.log(en0[1].address)

var Esp = require('../models/esp'); //UDP SERVER - GET DATA FROM ESPBOARD


var PORT = 33333;
var HOST = IPADDRESS;

var dgram = require('dgram');

var server = dgram.createSocket('udp4');
server.on('message', function (msg, rinfo) {
  console.log("server got: ".concat(msg, " from ").concat(rinfo.address, ":").concat(rinfo.port));
});
server.on('listening', function () {
  var address = server.address();
  console.log("server listening ".concat(address.address, ":").concat(address.port));
});
server.on('listening', function () {
  var address = server.address();
  console.table(['UDP Server listening on ', address.address + ':' + address.port]);
});
server.on('message', function (message, remote) {
  console.clear();
  var port = remote.port;

  if (port === PORT) {
    console.log("MSG", message, 'REMOTE:', remote);
    console.table([JSON.parse(message)]);
    console.log(remote.address + ':' + remote.port + ' - ' + message);
    console.log('type of remote port', _typeof(remote.port));
    insertToDB(JSON.parse(message));
  }
});
server.bind(PORT, HOST);

var insertToDB = function insertToDB(params) {
  var deviceType, frendlyName, ipAddress, relays, createdAt, isActive, esp, newESP;
  return regeneratorRuntime.async(function insertToDB$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          deviceType = params.deviceType, frendlyName = params.frendlyName, ipAddress = params.ipAddress, relays = params.relays, createdAt = params.createdAt, isActive = params.isActive; // destrukturyzacja

          console.log('data collected from ESP board by udp', params);
          esp = new Esp({
            deviceType: deviceType,
            frendlyName: frendlyName,
            ipAddress: ipAddress,
            relays: relays,
            createdAt: createdAt,
            isActive: isActive
          });
          _context.next = 5;
          return regeneratorRuntime.awrap(esp.save());

        case 5:
          newESP = _context.sent;
          console.log('ESP board data pushed to data base', newESP);

        case 7:
        case "end":
          return _context.stop();
      }
    }
  });
}; // router.post('/', async(req, res) => {
//     const esp = new Esp({
//         frendlyName: req.body.frendlyName,
//         ipAddress: req.body.ipAddress,
//         config: req.body.config,
//         state: req.body.state,
//     })
//     try {
//         const newEsp = await esp.save();
//         console.table([newEsp]);
//         res.render('saved', {
//             title: "Your ESP has been added to database",
//             esp: [newEsp]
//         })
//     } catch (error) {
//         res.status(400).json({ message: error.message })
//     }
// })


module.exports = router;