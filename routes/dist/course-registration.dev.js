"use strict";

var express = require('express');

var router = express.Router(); // var sys  = require('sys'),
//     http = require('http');

var myStorage = require('../storage/courseStorage');
/* GET home page. */


router.get('/', function (req, res, next) {
  res.render('courseregistration', {
    title: 'course registration'
  });
});
router.post('/registredcourse', function (req, res) {
  var _req$body = req.body,
      coursename = _req$body.coursename,
      coursecode = _req$body.coursecode,
      courseprice = _req$body.courseprice,
      courselength = _req$body.courselength;
  var courseData = {
    courseName: coursename,
    courseCode: coursecode,
    coursePrice: courseprice,
    courseLength: courselength
  }; // storage.addCourse(courseData);

  myStorage.addCourse(courseData);
  res.render('saved', {
    title: "Your COURSE has been createed",
    courses: [courseData]
  });
});
module.exports = router;