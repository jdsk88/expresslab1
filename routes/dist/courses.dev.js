"use strict";

// const auth = require('../middleware/auth');
var express = require('express');

var router = express.Router();

var Course = require('../models/courses'); // router.get('/', auth , async (req, res) => {


router.get('/', function _callee(req, res) {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          res.render('courseregistration', {
            title: 'course registration'
          });

        case 1:
        case "end":
          return _context.stop();
      }
    }
  });
});
router.post('/', function _callee2(req, res) {
  var course, newCourse;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          course = new Course({
            name: req.body.name,
            code: req.body.code,
            price: req.body.price,
            length: req.body.length
          });
          _context2.prev = 1;
          _context2.next = 4;
          return regeneratorRuntime.awrap(course.save());

        case 4:
          newCourse = _context2.sent;
          // const token = course.generateAuthToken();
          // res.header('x-access-token', token).send({
          //     validated: true
          // });
          console.log(newCourse);
          res.render('saved', {
            title: "Your COURSE has been createed",
            courses: [newCourse]
          });
          _context2.next = 12;
          break;

        case 9:
          _context2.prev = 9;
          _context2.t0 = _context2["catch"](1);
          res.status(400).json({
            message: _context2.t0.message
          });

        case 12:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[1, 9]]);
});
module.exports = router;