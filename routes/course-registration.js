var express = require('express');
var router = express.Router();
// var sys  = require('sys'),
//     http = require('http');

const myStorage = require('../storage/courseStorage');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('courseregistration', { title: 'course registration' });
});

router.post('/registredcourse', (req, res) => {
  const { coursename, coursecode, courseprice, courselength } = req.body;
  const courseData = { courseName: coursename, courseCode: coursecode, coursePrice: courseprice, courseLength: courselength }
  // storage.addCourse(courseData);
  myStorage.addCourse(courseData)

  res.render('saved', {
    title: "Your COURSE has been createed",
    courses: [courseData]
  })
})

module.exports = router;
