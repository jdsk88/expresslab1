const axios = require('axios');
// const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();
const os = require('os');

const getHostIP = os.networkInterfaces();
const {eth0 } = getHostIP;
const IPADDRESS = eth0[0].address;
// console.log(en0[1].address)

const Esp = require('../models/esp');

//UDP SERVER - GET DATA FROM ESPBOARD
const PORT = 33333;
const HOST = IPADDRESS;


const dgram = require('dgram');
const server = dgram.createSocket('udp4');

  
  server.on('message', (msg, rinfo) => {
    console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
  });
  
  server.on('listening', () => {
    const address = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
  });






server.on('listening', function() {
    const address = server.address();
    console.table(['UDP Server listening on ',${address.address} + ':' + ${address.port}]);
});


server.on('message', function(message, remote) {
    console.clear();
    const { port } = remote;
    if (port === PORT) {
        console.log("MSG", message, 'REMOTE:', remote)
        console.table([JSON.parse(message)]);
        console.log(remote.address + ':' + remote.port + ' - ' + message);
        console.log('type of remote port', typeof(remote.port))
        insertToDB(JSON.parse(message));
    }
});


server.bind(PORT, HOST);

const insertToDB = async(params) => {
    const { deviceType, frendlyName, ipAddress, relays, createdAt, isActive } = params; // destrukturyzacja
    console.log('data collected from ESP board by udp', params);
    const esp = new Esp({
        deviceType,
        frendlyName,
        ipAddress,
        relays,
        createdAt,
        isActive,
    });
    const newESP = await esp.save();
    console.log('ESP board data pushed to data base', newESP)
};

// router.post('/', async(req, res) => {
//     const esp = new Esp({
//         frendlyName: req.body.frendlyName,
//         ipAddress: req.body.ipAddress,
//         config: req.body.config,
//         state: req.body.state,
//     })
//     try {
//         const newEsp = await esp.save();
//         console.table([newEsp]);
//         res.render('saved', {
//             title: "Your ESP has been added to database",
//             esp: [newEsp]
//         })
//     } catch (error) {
//         res.status(400).json({ message: error.message })
//     }
// })

module.exports = router