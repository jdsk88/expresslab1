// const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

const Esp = require('../models/esp');

// router.get('/', auth , async (req, res) => {
router.get('/', async(req, res) => {
    res.render('esp', { title: 'course registration' });
})



router.post('/', async(req, res) => {
    const esp = new Esp({
        frendlyName: req.body.frendlyName,
        ipAddress: req.body.ipAddress,
        config: req.body.config,
        state: req.body.state,
    })
    try {
        const newEsp = await esp.save();
        // const token = course.generateAuthToken();
        // res.header('x-access-token', token).send({
        //     validated: true
        // });
        console.log(newEsp);
        res.render('saved', {
            title: "Your ESP has been added to database",
            esp: [newEsp]
        })
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
})

module.exports = router

//axios from udp await and