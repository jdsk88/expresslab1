// ArduinoJson - arduinojson.org
// Copyright Benoit Blanchon 2014-2020
// MIT License
//                                                    wersja stringu bez backslash
// https://arduinojson.org/v6/example/http-server/

#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESP8266mDNS.h>

// Replace with your network credentials
const char* ssid     = "Swiatlowod_DKD";
const char* password = "tosia1999";
unsigned int localPort = 8888;      // local port to listen on

// buffers for receiving and sending data
//char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1]; //buffer to hold incoming packet,
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1]; //buffer to hold incoming packet,
//char  ReplyBuffer[] = "acknowledged\r\n";       // a string to send back

char modul_name[] = "Switch-4";
char structName[] = "relays";
const char* rstate[2] = {"OFF", "ON"};

const int out1 = 14;   // GPIO14
const int out2 = 13;   // GPIO13
const int out3 = 12;   // GPIO12
const int out4 = 15;   // GPIO15

bool _relay[4];// = {true, true, true, true};


WiFiServer server(80);
WiFiUDP Udp;

void relaysUpdate()
{
  digitalWrite(out1, (_relay[0] & true ? LOW : HIGH)); // porównuje stan relay[n] z false(off) i wybiera wartość wyjścia out1
  digitalWrite(out2, (_relay[1] & true ? LOW : HIGH));
  digitalWrite(out3, (_relay[2] & true ? LOW : HIGH));
  digitalWrite(out4, (_relay[3] & true ? LOW : HIGH));
}

char jsonParse()
{
//  const char* jsonStr = "{\"Switch4\":[{\"state\":\"ON\"},{\"state\":\"OFF\"},{\"state\":\"ON\"},{\"state\":\"OFF\"}]}";//String(packetBuffer);
  const char* jsonStr = "{\"deviceType\":\"iSwitch-4\",\"frendlyName\":\"kitchen relays\",\"ipAddress\":\"192.168.0.101\",\"config\":false,\"relays\":[{\"state\":\"ON\"},{\"state\":\"ON\"},{\"state\":\"ON\"},{\"state\":\"ON\"}]}";
  char jsonString = JSON.stringify(jsonStr);
  Serial.println(jsonString);


  int pos[] = {0,0,0,0};
  int npos = 0;
  String sprin;

    Serial.println("Contents:");
    //Serial.println(packetBuffer);
    Serial.println(jsonStr);

  StaticJsonDocument<500> doc;

  DeserializationError  err = deserializeJson(doc, jsonStr);
//  DeserializationError  err = deserializeJson(doc, String(packetBuffer));  

  if(err) {
    Serial.print("Error: ");
    Serial.println(err.c_str());
//    return;
  }

  const char* _relays_ = doc["Switch-4"];
  char stan0 = doc["state"][0];
  char stan1 = doc["state"][1];
  char stan2 = doc["state"][2];
  char stan3 = doc["state"][3];

  Serial.println(_relays_);
  Serial.println(stan0);
  Serial.println(stan1);
  Serial.println(stan2);
  Serial.println(stan3);

  
/*  
  for (int i = 0; i <= 3; i++) {
    pos[i] = jsonStr.indexOf("state", npos);  
    sprin = jsonStr.substring(pos[i]+10, pos[i]+13);  // wersja stringu bez backslash 8 i 11
    npos = pos[i] + 1;
//    Serial.println(npos);
    Serial.println(sprin);
    if(sprin == "OFF"){
      _relay[i] = false;
    }else _relay[i] = true;
  }*/
 relaysUpdate();
}

char *jsonDataOut()
{
  char jsonData[300];
  const size_t capacity = JSON_ARRAY_SIZE(4) + 5*JSON_OBJECT_SIZE(1);
  DynamicJsonDocument doc(capacity);  
  // Create the "digital" array
/*  JsonArray array = doc.createNestedArray(structName);
  for (int pin = 12; pin < 16; pin++) {
    JsonObject relayNumber = array.createNestedObject();
    relayNumber["relay"] =pin-11;
    int value = digitalRead(pin);    
    relayNumber["state"] = rstate[value];
  }
 */
  JsonArray relay = doc.createNestedArray(modul_name);
  JsonObject relay_0 = relay.createNestedObject();
  relay_0["state"] = rstate[_relay[0]];
  JsonObject relay_1 = relay.createNestedObject();
  relay_1["state"] = rstate[_relay[1]];
  JsonObject relay_2 = relay.createNestedObject();
  relay_2["state"] = rstate[_relay[2]];
  JsonObject relay_3 = relay.createNestedObject();
  relay_3["state"] = rstate[_relay[3]];
  serializeJson(doc, jsonData);
  return (jsonData) ;
}

void setup() {
    pinMode(out1, OUTPUT);
    pinMode(out2, OUTPUT);
    pinMode(out3, OUTPUT);
    pinMode(out4, OUTPUT);
    digitalWrite(out1, HIGH);
    digitalWrite(out2, HIGH);
    digitalWrite(out3, HIGH);
    digitalWrite(out4, HIGH);  
  // Initialize serial port
  Serial.begin(115200);
  while (!Serial) continue;

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Start to listen
  server.begin();
  Udp.begin(localPort);
  
  Serial.println(F("Server is ready."));
  Serial.print(F("Please connect to http://"));
  Serial.println(WiFi.localIP());

  

  if (!MDNS.begin(modul_name)) { //Start mDNS
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }

  MDNS.addService("http","tcp",80);
  MDNS.addService("http", "tcp",45670);

}

void loop() {
 MDNS.update();  

// if there's data available, read a packet UDP
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.printf("Received packet of size %d from %s:%d\n    (to %s:%d, free heap = %d B)\n",
                  packetSize,
                  Udp.remoteIP().toString().c_str(), Udp.remotePort(),
                  Udp.destinationIP().toString().c_str(), Udp.localPort(),
                  ESP.getFreeHeap());
    // read the packet into packetBufffer
    int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    packetBuffer[n] = 0;
//    Serial.println("Contents:");
//    Serial.println(packetBuffer);
    jsonParse();
    // send a reply, to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(jsonDataOut());    
    Udp.endPacket();
  }

  // Wait for an incomming connection
 WiFiClient client = server.available();

  // Do we have a client?
  if (!client) return;

  Serial.println(F("New client"));

  // Read the request (we ignore the content in this example)
  while (client.available()) client.read();
/*
  // Allocate a temporary JsonDocument
  // Use arduinojson.org/v6/assistant to compute the capacity.
  StaticJsonDocument<500> doc;

  // Create the "digital" array
  JsonArray array = doc.createNestedArray(modul_name);
  for (int pin = 12; pin < 16; pin++) {
    JsonObject relayNumber = array.createNestedObject();
    relayNumber["relay"] =pin-11;
    int value = digitalRead(pin);    
    relayNumber["state"] = rstate[value];
  }

  Serial.print(F("Sending: "));
  //jsonBuffer = JSON.stringify(doc;);
  serializeJson(doc, jsonBuffer);
  Serial.println("jsonBuffer: ");  
  Serial.println(jsonBuffer);
  serializeJson(doc, Serial);
  Serial.println();

  // Write response headers
  client.println(F("HTTP/1.0 200 OK"));
  client.println(F("Content-Type: application/json"));
  client.println(F("Connection: close"));
  client.print(F("Content-Length: "));
  client.println(measureJsonPretty(doc));
  client.println();

  // Write JSON document
  serializeJsonPretty(doc, client);
*/
  // Disconnect
  client.stop();
}