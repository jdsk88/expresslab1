var PORT = 33333;
var HOST = '192.168.1.54';

var dgram = require('dgram');
var msg = new Buffer.from('{"deviceType":"iSwitch-4","frendlyName":"kitchen relays","ipAddress":"192.168.0.101","config":false,"relays":[{"relay":"1","state":"ON"},{"relay":"2","state":"ON"},{"relay":"3","state":"ON"},{"relay":"4","state":"ON"}]}');

var client = dgram.createSocket('udp4');
client.send(msg, 0, msg.length, PORT, HOST, function(err, bytes) {
    if (err) throw err;
    console.log('UDP msg sent to ' + HOST + ':' + PORT);
    client.close();
});